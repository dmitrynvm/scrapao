# Scrapao 

The application is a IMDb scraper with the following lifecycle.

Requirements: Python3, Bash.

1. Download and enter the repository
```bash
    git clone https://github.com/dmitrynvm/scrapao
    cd scrapao
```
2. Build and activate the environment
```bash
    make build
    source env/bin/activate
```
3. Run manually the app and check the json data
```bash
    make run
    cd data
    cat 1.imdb-original.json
    cat 2.imdb-penalized.json
    cat 3.imdb-rewarded.json
```
4. Run tests and ensure that everything is ok
```bash
    make test
```
5. Clean up the data and the environment
```bash
    make clean
    deactivate
    rm -rf env
```
