build:
	./script/build.sh

run:
	python3 scrapao/main.py

test:
	pytest --log-cli-level=INFO .

clean:
	./script/clean.sh
