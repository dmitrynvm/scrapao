import json
import functools
import os
import re
import requests
import os.path as osp
from bs4 import BeautifulSoup
from pathlib import Path
from typing import List, Dict
from urllib.parse import urljoin


def load(filename: str) -> List[Dict]:
    """
    Load data from the file in json format

    :param filename: path to the file in the json format
    :return: list of dictionaries loaded from the file
    """
    if osp.exists(filename):
        with open(filename, 'r') as f:
            outs = json.load(f)
    else:
        outs = []
    return outs


def save(data: List[Dict], filename: str):
    """
    Save data to the file in the json format creating the destination folder
    if it doesn't exist.

    :param data: list of dictionaries saved to the file
    :param filename: path to the file in json format
    """
    direname = Path(filename).parent.absolute()
    os.makedirs(direname, exist_ok=True)
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


def cache(filename='data/cache.json'):
    """
    Save the decorated function output data to the given file.
    Creates the folder if it doesn't exist.

    :param filename: path to the file in json format

    Example:
    @cache('data/imdb.json')
    def scrap(n: int):
        return [{'hello': 'world'}]
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            outs = func(*args, **kwargs)
            save(outs, filename)
            return outs
        return wrapper
    return decorator


def calc_reward(n: int) -> float:
    """
    Calculate reward for the oscars according to the given thresholds:
    - 1 or 2 oscars -> 0.3 point,
    - 3 or 5 oscars -> 0.5 point,
    - 6 to 10 oscars -> 1 point,
    - 10+ oscars -> 1.5 point.
    :param n: number of oscars
    :return: rating reward for the oscars
    """
    if n <= 0:
        out = 0.0
    elif 1 <= n <= 2:
        out = 0.3
    elif 3 <= n <= 5:
        out = 0.5
    elif 6 <= n <= 9:
        out = 1.0
    else:
        out = 1.5
    return out


@cache('data/1.imdb-original.json')
def scrap_imdb(n: int) -> List[Dict]:
    """
    Scrap from IMDb the given number of entries and caches them to given path
    in json format.
    1. Define the local variables and urls. headers variable is important for
    scraping individual movies.
    2. Scrap the movies and ratings columns from the top chart web page.
    3. Iterate through the movies and ratings extracting with regular
    expressions the desired values (title, score, votes, oscars).
    4. Save the scraped data to the given file.
    5. Return the scraped data.

    :param n: number of entries to be scraped
    :return: list of dictionaries to be saved in the file
    """
    outs = []
    headers = {
        'Accept-Language': 'lang=en-US',
        'User-agent': 'Mozilla/5.0'
    }
    url = 'http://www.imdb.com'
    chart_slug = 'chart/top'
    chart_link = urljoin(url, chart_slug)
    chart_page = requests.get(chart_link, headers=headers)
    chart_soup = BeautifulSoup(chart_page.text, 'lxml')

    movies = chart_soup.select('td.titleColumn')[:n]
    ratings = chart_soup.select('td.ratingColumn.imdbRating')[:n]
    for movie, rating in zip(movies, ratings):
        movie_slug = movie.select('a')[0]['href']
        movie_link = urljoin(url, movie_slug)
        movie_page = requests.get(movie_link, headers=headers)

        oscars_match = re.search('Won (?P<oscars>[0-9]+)', str(movie_page.text))
        score_match = re.search('(?P<score>[0-9]+(.[0-9]+)*) based on (?P<votes>[0-9]+(,[0-9]+)*) user ratings', str(rating))

        title = movie.get_text().strip().split('\n')[1].strip()
        score = float(score_match.group('score'))
        votes = int(score_match.group('votes').replace(',', ''))
        oscars = int(oscars_match.group('oscars')) if oscars_match else 0
        out = {
            'title': title,
            'score': score,
            'votes': votes,
            'oscars': oscars
        }
        outs += [out]
    return outs


@cache('data/1.imdb-original.json')
def scrap_or_load_imdb(n: int, filename: str = '') -> List[Dict]:
    """
    Scrap from IMDb the given number of entries and caches them to given path
    in json format. If the data is already cached -- load it and return.

    :param n: number of entries to be scraped
    :param filename: path to the file for entries to be saved
    :return: list of dictionaries to be saved in the file
    """
    if osp.exists(filename):
        outs = load(filename)
    else:
        outs = scrap_imdb(n)
    return outs


@cache('data/2.imdb-penalized.json')
def penalize(inps: List[Dict]):
    """
    Penalize the movies rating by the votes deviations from the maximal value.
    :param inps: table with the original movies' data
    :return: table with the penalized movies' data
    """
    outs = []
    max_votes = sorted(inps, key=lambda x: x['votes'], reverse=True)[0]['votes']
    for inp in inps:
        penalty = round((max_votes - inp['votes']) // 100000 * 0.1, 2)
        out = inp.copy()
        out['score'] = round(max(inp['score'] - penalty, 0.0), 2)
        outs += [out]
    return outs


@cache('data/3.imdb-rewarded.json')
def reward(inps: List[Dict]):
    """
    Reward the movies rating by the oscars obtained.
    :param inps: table with the original movies' data
    :return: table with the penalized movies' data
    """
    outs = []
    for inp in inps:
        out = inp.copy()
        out['score'] = round(min(inp['score'] + calc_reward(inp['oscars']), 10.0), 2)
        outs += [out]
    return outs


if __name__ == '__main__':
    original_data = scrap_or_load_imdb(20, 'data/1.imdb-original.json')
    print(original_data)
    penalized_data = penalize(original_data)
    print(penalized_data)
    rewarded_data = reward(penalized_data)
    print(rewarded_data)

