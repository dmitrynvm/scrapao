import pytest
from scrapao import scrap_or_load_imdb


@pytest.fixture(scope='session')
def imdb_data():
    """
    Return an imdb data scrapped from the web or loaded from the disk.
    """
    return scrap_or_load_imdb(20, 'data/1.imdb-original.json')
