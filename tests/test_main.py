from scrapao import calc_reward, penalize, reward


def test_scrap(imdb_data):
    assert len(imdb_data) == 20


def test_penalize(imdb_data):
    """
   Check sums of integral scores before and after penalize transform to be
   less than the assumed 5% threshold of the absolute value.
    """
    rates_before = [item['score'] for item in imdb_data]
    votes_before = [item['votes'] for item in imdb_data]
    rates_after = [item['score'] for item in penalize(imdb_data)]
    total_votes = max(votes_before) * len(votes_before)
    actual_votes = sum(votes_before)
    before_penalized = round(sum(rates_before) - (total_votes - actual_votes) // 100000 * 0.1, 2)
    after_penalized = sum(rates_after)
    epsilon = max(before_penalized, after_penalized) * 0.05
    assert before_penalized - after_penalized < epsilon


def test_reward(imdb_data):
    """
   Check sums of scores before and after reward transform to be less than
   the assumed 5% threshold of the absolute value.
    """
    rewards_before = sum([calc_reward(item['oscars']) for item in imdb_data])
    rewards_after = sum([b['score'] - a['score'] for a, b in zip(imdb_data, reward(imdb_data))])
    epsilon = max(rewards_before, rewards_after) * 0.05
    assert rewards_before - rewards_after < epsilon
